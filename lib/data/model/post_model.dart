import 'package:flutter/material.dart';

import '../../domain/entity/post.dart';

class PostModel extends Post {
  PostModel(
      {@required int id,
      @required String title,
      @required String content,
      @required String coverImage})
      : super(id: id, title: title, content: content, coverImage: coverImage);

  factory PostModel.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> titleJson = json['title'];
    Map<String, dynamic> contentJson = json['content'];
    Map<String, dynamic> featuredImageJson = json['uagb_featured_image_src'];
    List<dynamic> mediumJson = featuredImageJson['medium_large'];
    return PostModel(
        id: json['id'],
        title: titleJson['rendered'],
        content: contentJson['rendered'],
        coverImage: mediumJson[0]);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'title': title,
      'content': content,
      'coverImage': coverImage
    };
  }
}
