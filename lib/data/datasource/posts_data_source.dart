import 'dart:convert';

import 'package:http/http.dart' as http;

import '../../core/error/exception.dart';
import '../model/post_model.dart';

abstract class PostsDataSource {
  Future<List<PostModel>> getMoneyPosts(int page, int perPage);
  Future<List<PostModel>> getMindsetPosts(int page, int perPage);
}

class PostsDataSourceImpl extends PostsDataSource {
  final http.Client client;

  PostsDataSourceImpl(this.client);

  @override
  Future<List<PostModel>> getMoneyPosts(int page, int perPage) =>
      _getPosts(client.get(_url(7, page, perPage)));

  @override
  Future<List<PostModel>> getMindsetPosts(int page, int perPage) =>
      _getPosts(client.get(_url(8, page, perPage)));

  String _url(int tag, int page, int perPage) =>
      'http://youngandinvesting.com/wp-json/wp/v2/posts?tags[]=$tag&page=$page&per_page=$perPage';

  Future<List<PostModel>> _getPosts(
      Future<http.Response> clientResponse) async {
    final response = await clientResponse;
    if (response.statusCode == 200) {
      return jsonDecode(response.body)
          .map<PostModel>((postJson) => PostModel.fromJson(postJson))
          .toList();
    } else {
      throw ServerException();
    }
  }
}
