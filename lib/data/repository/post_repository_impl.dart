import 'package:dartz/dartz.dart';

import '../../core/error/exception.dart';
import '../../core/error/failure.dart';
import '../../core/platform/network_info.dart';
import '../../domain/entity/post.dart';
import '../../domain/repository/posts_repository.dart';
import '../datasource/posts_data_source.dart';

class PostsRepositoryImpl extends PostsRepository {
  final PostsDataSource postsDataSource;
  final NetworkInfo networkInfo;

  PostsRepositoryImpl(this.postsDataSource, this.networkInfo);

  @override
  Future<Either<ServerFailure, List<Post>>> getMindsetPosts(
          int page, int perPage) =>
      _getPosts(postsDataSource.getMindsetPosts(page, perPage));

  @override
  Future<Either<ServerFailure, List<Post>>> getMoneyPosts(
          int page, int perPage) =>
      _getPosts(postsDataSource.getMoneyPosts(page, perPage));

  Future<Either<ServerFailure, List<Post>>> _getPosts(
      Future<List<Post>> dataSource) async {
    if (await networkInfo.isConnected) {
      try {
        final posts = await dataSource;
        return Right(posts);
      } on ServerException {
        return Left(ServerFailure('Whoops! Something went wrong'));
      }
    } else {
      return Left(ServerFailure('No internet connection'));
    }
  }
}
