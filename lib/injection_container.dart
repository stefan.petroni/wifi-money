import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import 'core/platform/network_info.dart';
import 'data/datasource/posts_data_source.dart';
import 'data/repository/post_repository_impl.dart';
import 'domain/repository/posts_repository.dart';
import 'domain/usecase/get_mindset_posts.dart';
import 'domain/usecase/get_money_posts.dart';
import 'presentation/bloc/posts_bloc.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // Bloc
  sl.registerFactory(() => PostsBloc(sl(), sl()));

  // Use case
  sl.registerLazySingleton(() => GetMoneyPosts(sl()));
  sl.registerLazySingleton(() => GetMindsetPosts(sl()));

  // Repository
  sl.registerLazySingleton<PostsRepository>(
      () => PostsRepositoryImpl(sl(), sl()));

  // Data source
  sl.registerLazySingleton<PostsDataSource>(() => PostsDataSourceImpl(sl()));

  // Core
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));

  // External
  sl.registerLazySingleton(() => http.Client());
  sl.registerLazySingleton(() => DataConnectionChecker());
}
