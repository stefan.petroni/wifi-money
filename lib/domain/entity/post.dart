import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Post extends Equatable {
  final int id;
  final String title;
  final String content;
  final String coverImage;

  Post(
      {@required this.id,
      @required this.title,
      @required this.content,
      @required this.coverImage})
      : super();

  @override
  List<Object> get props => [id];
}

enum PostType { money, mindset }
