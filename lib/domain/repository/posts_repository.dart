import 'package:dartz/dartz.dart';

import '../../core/error/failure.dart';
import '../entity/post.dart';

abstract class PostsRepository {
  Future<Either<ServerFailure, List<Post>>> getMoneyPosts(int page, int perPage);
  Future<Either<ServerFailure, List<Post>>> getMindsetPosts(int page, int perPage);
}
