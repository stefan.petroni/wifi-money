import 'package:dartz/dartz.dart';

import '../../core/error/failure.dart';
import '../entity/post.dart';
import '../repository/posts_repository.dart';
import 'get_posts.dart';

class GetMindsetPosts extends GetPosts {
  GetMindsetPosts(PostsRepository postsRepository) : super(postsRepository);

  @override
  Future<Either<ServerFailure, List<Post>>> execute(Params params) async {
    return await postsRepository.getMindsetPosts(params.page, params.perPage);
  }
}
