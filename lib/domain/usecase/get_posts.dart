import 'package:flutter/material.dart';

import '../../core/usecase/usecase.dart';
import '../entity/post.dart';
import '../repository/posts_repository.dart';

abstract class GetPosts implements UseCase<List<Post>, Params> {
  final PostsRepository postsRepository;

  GetPosts(this.postsRepository);
}

class Params {
  final int page;
  final int perPage;

  Params({@required this.page, @required this.perPage});
}
