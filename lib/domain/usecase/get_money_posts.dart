import 'package:dartz/dartz.dart';

import '../../core/error/failure.dart';
import '../entity/post.dart';
import '../repository/posts_repository.dart';
import 'get_posts.dart';

class GetMoneyPosts extends GetPosts {
  GetMoneyPosts(PostsRepository postsRepository) : super(postsRepository);

  @override
  Future<Either<ServerFailure, List<Post>>> execute(Params params) async {
    return await postsRepository.getMoneyPosts(params.page, params.perPage);
  }
}
