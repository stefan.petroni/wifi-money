import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:wifi_money/core/error/failure.dart';
import 'package:wifi_money/domain/usecase/get_posts.dart';

import '../../domain/entity/post.dart';
import '../../domain/usecase/get_mindset_posts.dart';
import '../../domain/usecase/get_money_posts.dart';

part 'posts_event.dart';
part 'posts_state.dart';

class PostsBloc extends Bloc<PostsEvent, PostsState> {
  final GetMoneyPosts getMoneyPostsUseCase;
  final GetMindsetPosts getMindsetPostsUseCase;

  int page = 1;
  int perPage = 10;

  PostsBloc(this.getMoneyPostsUseCase, this.getMindsetPostsUseCase);

  @override
  PostsState get initialState => Initial();

  void getPosts(PostType postType) {
    add(postType == PostType.money
        ? GetMoneyPostsEvent()
        : GetMindsetPostsEvent());
  }

  @override
  Stream<Transition<PostsEvent, PostsState>> transformEvents(
      Stream<PostsEvent> events, transitionFn) {
    return page == 1
        ? super.transformEvents(events, transitionFn)
        : super.transformEvents(
            events.debounceTime(const Duration(milliseconds: 500)),
            transitionFn);
  }

  @override
  Stream<PostsState> mapEventToState(
    PostsEvent event,
  ) async* {
    final currentState = state;

    if (event is GetMoneyPostsEvent && !_hasReachedMax(currentState)) {
      final either = await getMoneyPostsUseCase
          .execute(Params(page: page++, perPage: perPage));
      yield either.fold((failure) => _failureState(currentState, failure),
          (posts) => _successState(currentState, posts, perPage));
    } else if (event is GetMindsetPostsEvent && !_hasReachedMax(currentState)) {
      final either = await getMindsetPostsUseCase
          .execute(Params(page: page++, perPage: perPage));
      yield either.fold((failure) => _failureState(currentState, failure),
          (posts) => _successState(currentState, posts, perPage));
    }
  }
}

PostsState _successState(
    PostsState currentState, List<Post> posts, int perPage) {
  return currentState is Loaded
      ? currentState.copyWith(
          posts: currentState.posts + posts,
          hasReachedMax: posts.length < perPage)
      : Loaded(posts: posts, hasReachedMax: posts.length < perPage);
}

PostsState _failureState(PostsState currentState, ServerFailure failure) {
  if (currentState is Loaded) {
    return currentState.posts.isEmpty
        ? Error(failure.message)
        : currentState.copyWith(hasReachedMax: true);
  } else {
    return Error(failure.message);
  }
}

bool _hasReachedMax(PostsState state) => state is Loaded && state.hasReachedMax;
