part of 'posts_bloc.dart';

abstract class PostsState extends Equatable {
  const PostsState();

  @override
  List<Object> get props => [];
}

class Initial extends PostsState {}

class Loaded extends PostsState {
  final List<Post> posts;
  final bool hasReachedMax;

  Loaded({
    @required this.posts,
    @required this.hasReachedMax,
  });

  Loaded copyWith({List<Post> posts, bool hasReachedMax}) {
    Loaded state = Loaded(
        posts: posts ?? this.posts,
        hasReachedMax: hasReachedMax ?? hasReachedMax);
    return state;
  }

  @override
  List<Object> get props => [posts, hasReachedMax];
}

class Error extends PostsState {
  final String message;

  Error(this.message);
}
