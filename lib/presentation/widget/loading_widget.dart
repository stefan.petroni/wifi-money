import 'package:flutter/widgets.dart';

import 'app_widget.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: Center(
        child: PlatformIndicator(),
      ),
    );
  }
}
