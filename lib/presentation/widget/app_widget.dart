import 'dart:io' show Platform;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PlatformApp extends StatelessWidget {
  final String title;
  final Widget home;
  final String initialRoute;
  final Map<String, Widget Function(BuildContext context)> routes;

  PlatformApp({Key key, this.title, this.home, this.routes, this.initialRoute})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Platform.isIOS
      ? CupertinoApp(
          title: title,
          home: home,
          theme: CupertinoThemeData(
              scaffoldBackgroundColor: Colors.white,
              textTheme: CupertinoTextThemeData(
                  navTitleTextStyle: _navTitleTextStyle,
                  primaryColor: Colors.white),
              barBackgroundColor: Colors.cyan.shade600),
          initialRoute: initialRoute != null ? initialRoute : '',
          routes: routes != null ? routes : {},
        )
      : MaterialApp(
          title: title,
          home: home,
          theme: ThemeData(
            primaryColor: Colors.cyan.shade600,
            accentColor: Colors.cyan.shade600,
            primaryTextTheme:
                TextTheme(headline6: TextStyle(color: Colors.white)),
          ),
          initialRoute: initialRoute != null ? initialRoute : '',
          routes: routes != null ? routes : {},
        );

  final TextStyle _navTitleTextStyle = TextStyle(
    inherit: false,
    fontFamily: '.SF Pro Text',
    fontSize: 17.0,
    fontWeight: FontWeight.w600,
    letterSpacing: -0.41,
    color: Colors.white,
  );
}

class PlatformPage extends StatelessWidget {
  final Text title;
  final Widget body;
  final Color toolbarColor;

  const PlatformPage({Key key, this.title, this.body, this.toolbarColor})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Platform.isIOS
      ? CupertinoPageScaffold(
          child: body,
          navigationBar: CupertinoNavigationBar(
            backgroundColor: toolbarColor != null
                ? toolbarColor
                : Theme.of(context).appBarTheme.color,
            middle: title,
          ),
        )
      : Scaffold(
          body: body,
          appBar: AppBar(
            backgroundColor: toolbarColor != null
                ? toolbarColor
                : Theme.of(context).appBarTheme.color,
            title: title,
          ),
        );
}

class PlatformTabPage extends StatelessWidget {
  final Text title;
  final Widget body;
  final Color toolbarColor;
  final List<Widget> toolbarActions;
  final Widget navBarLeading;
  final Widget navBarTrailing;
  final List<BottomNavigationBarItem> items;
  final ValueChanged<int> onTap;
  final int currentIndex;

  const PlatformTabPage(
      {Key key,
      this.title,
      this.body,
      this.toolbarColor,
      this.toolbarActions,
      this.navBarLeading,
      this.navBarTrailing,
      @required this.items,
      @required this.onTap,
      @required this.currentIndex})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Platform.isIOS
      ? CupertinoTabScaffold(
          tabBar: CupertinoTabBar(
            items: items,
            onTap: onTap,
            activeColor: Colors.white,
            inactiveColor: Colors.white54,
          ),
          tabBuilder: (BuildContext context, int index) => CupertinoTabView(
                builder: (BuildContext context) => CupertinoPageScaffold(
                  child: body,
                  navigationBar: CupertinoNavigationBar(
                    backgroundColor: toolbarColor != null
                        ? toolbarColor
                        : Theme.of(context).appBarTheme.color,
                    middle: title,
                    leading: navBarLeading,
                    trailing: navBarTrailing,
                  ),
                ),
              ))
      : Scaffold(
          body: body,
          appBar: AppBar(
            backgroundColor: toolbarColor != null
                ? toolbarColor
                : Theme.of(context).appBarTheme.color,
            title: title,
            actions: toolbarActions != null ? toolbarActions : [],
          ),
          bottomNavigationBar: BottomNavigationBar(
            items: items,
            onTap: onTap,
            currentIndex: currentIndex,
          ),
        );
}

class PlatformIndicator extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Platform.isIOS
      ? CupertinoActivityIndicator()
      : CircularProgressIndicator();
}
