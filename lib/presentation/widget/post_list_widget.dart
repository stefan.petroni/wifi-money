import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wifi_money/presentation/widget/app_widget.dart';

import '../../domain/entity/post.dart';
import '../page/PostPage.dart';

class PostListWidget extends StatelessWidget {
  final List<Post> posts;
  final bool hasReachedMax;
  final ScrollController scrollController;

  const PostListWidget(
      {@required this.posts,
      @required this.hasReachedMax,
      @required this.scrollController});

  @override
  Widget build(BuildContext context) => ListView.builder(
        itemCount: hasReachedMax ? posts.length : posts.length + 1,
        itemBuilder: (context, index) => index >= posts.length
            ? _loadingItem(context)
            : _postItem(context, posts[index], index == posts.length - 1),
        controller: scrollController,
      );

  Widget _postItem(BuildContext context, Post post, bool isLast) =>
      GestureDetector(
        child: Container(
          margin: EdgeInsets.only(
              top: 8, left: 8, right: 8, bottom: isLast ? 8 : 0),
          child: Stack(
            children: <Widget>[
              AspectRatio(
                aspectRatio: 10 / 4,
                child: CachedNetworkImage(
                  imageUrl: post.coverImage,
                  fit: BoxFit.cover,
                ),
              ),
              AspectRatio(
                  aspectRatio: 10 / 4,
                  child: Container(
                    padding: EdgeInsets.all(16),
                    color: Colors.black.withOpacity(0.6),
                    child: Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        post.title,
                        style: TextStyle(color: Colors.white, fontSize: 24),
                      ),
                    ),
                  ))
            ],
          ),
        ),
        onTap: () {
          Navigator.of(context, rootNavigator: true)
              .pushNamed(PostPage.routeName, arguments: post);
        },
      );

  Widget _loadingItem(BuildContext context) => Container(
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 8, bottom: 16),
        child: Center(
          child: SizedBox(
            width: 33,
            height: 33,
            child: PlatformIndicator(),
          ),
        ),
      );
}
