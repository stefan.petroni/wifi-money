import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:share/share.dart';

import '../../domain/entity/post.dart';
import '../../injection_container.dart';
import '../bloc/posts_bloc.dart';
import '../widget/app_widget.dart';
import '../widget/loading_widget.dart';
import '../widget/message_widget.dart';
import '../widget/post_list_widget.dart';

class PostListPage extends StatefulWidget {
  @override
  _PostListPageState createState() => _PostListPageState();
}

class _PostListPageState extends State<PostListPage> {
  final List<StatefulWidget> pages = [_MoneyPosts(), _MindsetPosts()];

  int _currentIndex = 0;

  static const TextStyle optionStyle =
      TextStyle(fontSize: 14, fontWeight: FontWeight.w500);

  @override
  Widget build(BuildContext context) {
    return PlatformTabPage(
      title: Text('WIFI Money'),
      toolbarActions: <Widget>[
        IconButton(
            icon: Icon(
              Icons.share,
              color: Colors.white,
            ),
            onPressed: _onShare)
      ],
      navBarTrailing: GestureDetector(
        child: Icon(
          CupertinoIcons.share,
          color: Colors.white,
        ),
        onTap: _onShare,
      ),
      body: IndexedStack(
        index: _currentIndex,
        children: pages,
      ),
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.attach_money),
          title: Text(
            "Money",
            style: optionStyle,
          ),
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text(
              "Mindset",
              style: optionStyle,
            ))
      ],
      onTap: (_onItemTapped),
      currentIndex: _currentIndex,
    );
  }

  void _onItemTapped(int index) {
    if (index != _currentIndex) {
      setState(() => _currentIndex = index);
    }
  }

  void _onShare() {
    final url = Platform.isIOS
        ? 'https://play.google.com/store/apps/details?id=com.wifi.money'
        : 'https://play.google.com/store/apps/details?id=com.wifi.money';
    Share.share(url);
  }
}

class _MoneyPosts extends StatefulWidget {
  @override
  _PageBodyState createState() => _PageBodyState(PostType.money);
}

class _MindsetPosts extends StatefulWidget {
  @override
  _PageBodyState createState() => _PageBodyState(PostType.mindset);
}

class _PageBodyState extends State<StatefulWidget> {
  final _postsBloc = sl<PostsBloc>();
  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;

  final PostType postType;

  _PageBodyState(this.postType);

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
  }

  @override
  Widget build(BuildContext context) => BlocBuilder(
        bloc: _postsBloc,
        builder: (context, PostsState state) {
          if (state is Loaded) {
            return PostListWidget(
              posts: state.posts,
              hasReachedMax: state.hasReachedMax,
              scrollController: _scrollController,
            );
          } else if (state is Error) {
            return MessageWidget(state.message);
          } else {
            _postsBloc.getPosts(postType);
            return LoadingWidget();
          }
        },
      );

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _postsBloc.getPosts(postType);
    }
  }
}
