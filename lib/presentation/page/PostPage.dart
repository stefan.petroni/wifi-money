import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../domain/entity/post.dart';
import '../widget/app_widget.dart';

class PostPage extends StatefulWidget {
  static const routeName = '/postPage';

  @override
  State<StatefulWidget> createState() {
    return PostPageState();
  }
}

class PostPageState extends State<PostPage> {
  WebViewController _controller;

  @override
  Widget build(BuildContext context) {
    final Post post = ModalRoute.of(context).settings.arguments;
    return PlatformPage(
      title: Text(post.title),
      body: WebView(
        javascriptMode: JavascriptMode.unrestricted,
        onWebViewCreated: (WebViewController controller) {
          _controller = controller;
          _loadHtml(post);
        },
        navigationDelegate: (NavigationRequest request) =>
            _launchURL(request.url),
        onWebResourceError: (WebResourceError error) {
          print(error.description);
        },
      ),
    );
  }

  _loadHtml(Post post) async {
    _controller.loadUrl(Uri.dataFromString(
      post.content,
      mimeType: 'text/html',
      encoding: Encoding.getByName('UTF-8'),
    ).toString());
  }

  _launchURL(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
