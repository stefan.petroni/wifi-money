import 'package:flutter/material.dart';
import 'package:wifi_money/presentation/page/PostPage.dart';

import 'injection_container.dart' as di;
import 'presentation/page/PostListPage.dart';
import 'presentation/widget/app_widget.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return PlatformApp(
      title: 'WIFI Money',
      initialRoute: '/',
      routes: {
        '/': (context) => PostListPage(),
        PostPage.routeName: (context) => PostPage(),
      },
    );
  }
}
